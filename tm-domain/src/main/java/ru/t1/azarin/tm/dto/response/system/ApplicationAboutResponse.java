package ru.t1.azarin.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ApplicationAboutResponse extends AbstractResponse {

    @Nullable
    private String name;

    @Nullable
    private String email;

    @Nullable
    private String host;

}
