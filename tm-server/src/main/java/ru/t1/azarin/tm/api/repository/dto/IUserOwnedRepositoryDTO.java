package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

}
