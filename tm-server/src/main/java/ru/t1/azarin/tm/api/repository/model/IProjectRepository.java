package ru.t1.azarin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    List<Project> findAll(@NotNull String userId, @NotNull Sort sort);

}