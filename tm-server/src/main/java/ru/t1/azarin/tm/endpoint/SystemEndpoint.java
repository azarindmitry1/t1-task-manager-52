package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.IServiceLocator;
import ru.t1.azarin.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.azarin.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.azarin.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.azarin.tm.dto.response.system.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.azarin.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        //response.setName(getPropertyService().getAuthorName());
        //response.setEmail(getPropertyService().getAuthorEmail());
        response.setHost(getPropertyService().getServerHost());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(getPropertyService().getApplicationVersion());
        return response;
    }

}
