package ru.t1.azarin.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
