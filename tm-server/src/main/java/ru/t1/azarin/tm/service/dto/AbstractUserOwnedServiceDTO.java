package ru.t1.azarin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R> implements IUserOwnedServiceDTO<M> {

    public AbstractUserOwnedServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
