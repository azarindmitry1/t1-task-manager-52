package ru.t1.azarin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@Nullable M model);

}
